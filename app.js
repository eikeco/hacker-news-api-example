var httpRequest = new XMLHttpRequest();
httpRequest.open('GET', 'https://hacker-news.firebaseio.com/v0/topstories.json', true);

function reqListener2 () {
    var thisData = JSON.parse(this.responseText),
        html = '<li><span>' + thisData.score + ' points</span><br><a href="' + thisData.url + '">' +  thisData.title + '</a><a href="https://news.ycombinator.com/user?id=' + thisData.by + '">By: ' + thisData.by + '</a></li>';

    document.getElementById('root').insertAdjacentHTML('beforeend', html);
}

function reqListener () {

    var rawData = JSON.parse(this.responseText),
        data = rawData.slice(0, 5);

    console.log(rawData);

    [].forEach.call(data, function(item) {
        var httpRequest = new XMLHttpRequest();
        httpRequest.open('GET', 'https://hacker-news.firebaseio.com/v0/item/' + item + '.json?print=pretty');
        httpRequest.addEventListener("load", reqListener2);
        httpRequest.send(null);
    });

}

httpRequest.addEventListener("load", reqListener);
httpRequest.send(null);